$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri('org/dppware/cucumber/miPrimerTest.feature');
formatter.feature({
  "line": 1,
  "name": "Probar calculadora",
  "description": "Mediante este test voy a\r\nhacer pruebas con dos numeros a\r\nver como se comporta mi aplicacion.",
  "id": "probar-calculadora",
  "keyword": "Feature"
});
formatter.scenario({
  "line": 6,
  "name": "Sumatorio de dos numeros",
  "description": "",
  "id": "probar-calculadora;sumatorio-de-dos-numeros",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 7,
  "name": "dos numeros 2,3",
  "keyword": "Given "
});
formatter.step({
  "line": 8,
  "name": "sumo los dos numeros",
  "keyword": "When "
});
formatter.step({
  "line": 9,
  "name": "multiplico el resultado por 3",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "resultado debe ser 15",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "2",
      "offset": 12
    },
    {
      "val": "3",
      "offset": 14
    }
  ],
  "location": "CalculosTest.given_dos_numeros(int,int)"
});
formatter.result({
  "duration": 197421424,
  "status": "passed"
});
formatter.match({
  "location": "CalculosTest.sumo_los_dos_numeros()"
});
formatter.result({
  "duration": 37004,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "3",
      "offset": 28
    }
  ],
  "location": "CalculosTest.multiplico_el_resultado_por(int)"
});
formatter.result({
  "duration": 260660,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "15",
      "offset": 19
    }
  ],
  "location": "CalculosTest.resultado_debe_ser(int)"
});
formatter.result({
  "duration": 3689517,
  "status": "passed"
});
});