<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class ClientesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

DB::table('clientes')->insert([
        	'dni'=>64546554,
            'password'=>'hola',
        	'nombre'=>'maria',
        	'direccion'=>'mata',
        	'telefono'=>6584400,
        	]);
DB::table('clientes')->insert([
        	'dni'=>6534234,
            'password'=>'adios',
        	'nombre'=>'luis',
        	'direccion'=>'cervantes',
        	'telefono'=>63434535,
        	]);
    }
}
