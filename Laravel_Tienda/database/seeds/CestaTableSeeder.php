<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class CestaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('cestas')->insert([
        	'id'=>1,
        	'precio'=>800,
        	]);

        DB::table('cestas')->insert([
        	'id'=>2,
        	'precio'=>300,
        	]);

    }
}
