<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
    	$this->call(ClientesTableSeeder::class);
    	$this->call(CestaTableSeeder::class);
    	$this->call(ProductosTableSeeder::class);
    	$this->call(Cesta_ProductosTableSeeder::class);

    }
}
