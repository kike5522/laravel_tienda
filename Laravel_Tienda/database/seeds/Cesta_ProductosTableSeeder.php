<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class Cesta_ProductosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cesta__productos')->insert([
          	'id_cesta'=>2,
          	'id_producto'=>10,	
        	]);
        DB::table('cesta__productos')->insert([
           	'id_cesta'=>2,
        	'id_producto'=>15,
        	]);
        DB::table('cesta__productos')->insert([
           	'id_cesta'=>1,
        	'id_producto'=>5,
        	]);
        DB::table('cesta__productos')->insert([
            	        	'id_cesta'=>1,
        	'id_producto'=>13,
        	]);
       
    }
}
