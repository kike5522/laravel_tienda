<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Clientes extends Model{
    protected $table = 'clientes';  
   	protected $primaryKey = 'dni';

   	public $timestamps = false;
    //return $this->belongsTo('App\Cesta')
}
