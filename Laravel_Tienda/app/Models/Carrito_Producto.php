<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Carrito_Producto extends Model{
	
   // return $this->hasMany('App\Productos');
   // return $this->belongTo('App\Cesta');

	protected $table='carrito_producto';
	protected $primaryKey='id_carrito, id_producto';

	public $timestamps = false;
}
