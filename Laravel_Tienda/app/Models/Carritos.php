<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Carritos extends Model
{
	protected $table = 'carritos';  
    protected $primaryKey = 'id, id_cliente';

    public $timestamps = false;
    /*return $this->hasOne('App\Clientes');
    return $this->belongsTo('App\Cesta_Productos')*/
}
