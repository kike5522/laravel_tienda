<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::get('/', function () {
    return view('welcome');
});

/*Route::get('tienda', function(){
	return view ('main');
});*/
/*
Route::get('mujer', function(){
	return view ('mujer');
});
Route::get('hombre', function(){
	return view ('hombre');
});
Route::get('ninio', function(){
	return view ('ninio');
});
*/
Route::get('tienda','HomeController@getLogin');
Route::get('mujer','MujerController@getBolsos');
Route::get('hombre','HombreController@getBolsos');
Route::get('ninio','NinioController@getBolsos');
Route::get('carrito', 'CarritoController@getCarro');

Route::post('tienda','HomeController@postIndex');
Route::post('mujer', 'MujerController@postAdd');
Route::post('hombre', 'HombreController@postAdd');
Route::post('ninio', 'NinioController@postAdd');
Route::post('carrito', 'CarritoController@postComprar');

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    //
});
