<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Productos;
use App\Models\Carrito_Producto;
use App\Models\Carritos;

class HombreController extends Controller{
	public function getBolsos(){

		$bolsos=Productos::where('categoria','LIKE','hombre')->get();
    $unidades_total=Carritos::orderBy('id', 'desc')->pluck('unidades_total')->first();
    	
    return \View::make('hombre')->with('bolsos', $bolsos)->with('unidades_total', $unidades_total);
  }

  	public function postAdd(){

  		$id_carrito = Carritos::orderBy('id', 'desc')->pluck('id')->first();
  		$id_producto = \Input::get('id_bolso');

  		$bolso = Productos::where('id', '=', $id_producto)->first();

      $carrito= Carritos::where('id', '=', $id_carrito)-> first();
      $precio_total = $carrito -> precio_total;
      $unidades_total = $carrito -> unidades_total;

  		/*echo $id_carrito;
  		echo $id_producto;*/

  		if($prodCarrito= Carrito_Producto::where('id_carrito','=', $id_carrito)->where('id_producto','=',$id_producto)->first()){

  			$unidades = ($prodCarrito -> unidades) + 1;
  			$subtotal = ($bolso -> precio) * $unidades;

        $precio_total = $precio_total + ($bolso -> precio);
        $unidades_total = $unidades_total + 1;
        
  			//echo $subtotal;

  			Carrito_Producto::where('id_carrito','=', $id_carrito)->where('id_producto','=',$id_producto)
  			->update(array('unidades' => $unidades , 'subtotal' => $subtotal));	

        Carritos::where('id', '=', $id_carrito)-> update(array('precio_total' => $precio_total , 'unidades_total' => $unidades_total));

  		}else{
  			$prodCarrito = new Carrito_Producto;
  			$prodCarrito -> id_carrito = $id_carrito;
  			$prodCarrito -> id_producto = $id_producto;
  			$prodCarrito -> unidades = 1;
  			$prodCarrito -> subtotal= $bolso -> precio;
  	
  			$prodCarrito -> save();

        $precio_total = $precio_total + ($bolso -> precio);
        $unidades_total = $unidades_total + 1;

        Carritos::where('id', '=', $id_carrito)-> update(array('precio_total' => $precio_total , 'unidades_total' => $unidades_total));


  		}
  		$bolsos=Productos::where('categoria','LIKE','hombre')->get();
    	return \View::make('hombre')->with('bolsos', $bolsos)->with('unidades_total', $unidades_total);

  	}

}
