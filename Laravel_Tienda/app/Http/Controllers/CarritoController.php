<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Productos;
use App\Models\Carrito_Producto;
use App\Models\Carritos;
use App\Models\Clientes;

class CarritoController extends Controller{
	
	public function getCarro(){
		$carrito=Carritos::orderBy('id', 'desc')->first();
		$cliente= Clientes::where('dni', 'LIKE', $carrito -> id_cliente)->first();
		$car_prods= Carrito_Producto::where('id_carrito', '=', $carrito -> id)-> get();
		
		$lineas= array();

		foreach ($car_prods as $car_prod) {
			$producto= Productos::where('id', 'LIKE', $car_prod-> id_producto)->first();
			$linea = array('img' => $producto -> imagen, 'nombre' => $producto -> nombre, 'precio' => $producto -> precio,
				            'unidades' => $car_prod -> unidades, 'subtotal' => $car_prod -> subtotal);
			//print_r($linea);
			$lineas[]= $linea;
		}
		//print_r($lineas);
		
		return \View::make('carrito')->with('carrito', $carrito)->with('cliente', $cliente)->with('lineas', $lineas);
  	}

  	public function postComprar(){

  	}

}
