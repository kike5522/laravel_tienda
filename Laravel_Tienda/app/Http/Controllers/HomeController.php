<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Clientes;
use App\Models\Productos;
use App\Models\Carritos;

class HomeController extends Controller
{
    public function postIndex(){
            $dni= \Input::get('dni');
            $password =Input::get('password');

            if($usuario= Clientes::where('dni','LIKE','%'.$dni.'%')->where('password','LIKE','%'.$password.'%')->first()){
                //crear carrito con dni usuario  e id autoincremento
                $carrito= new Carritos();
                $carrito ->id_cliente = $dni;
                $carrito->save();

                $bolsos=Productos::where('categoria','LIKE','mujer')->get();
                //return \View::make('mujer')->with('bolsos', $bolsos)->with('carrito',$carrito->id);
                return \View::make('main_in');
            }else{
                return \View::make('main');
            }  
    
    }
 
    public function getLogin(){
        return \View::make('main');

    }
 
    public function getLogout(){
        Auth::logout();
 
        return \View::make('main');
    }
}
