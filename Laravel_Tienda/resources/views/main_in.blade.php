<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title></title>

    <!-- Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!--<link href='https://fonts.googleapis.com/css?family=Signika' rel='stylesheet' type='text/css'> -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Tangerine">
    <style>
      body {
        font-family: 'Tangerine', serif;
        font-size: 48px;
      }
    </style>

    <!-- Styles -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>


</head>
<body id="app-layout" background="img/fondo3.jpg">
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <!-- Branding Image -->
                <img alt="Brand" src="img/logo.jpg" height="60" width="75">
                
            </div>

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                <li><p class="navbar-text"> </p></li>
                    <li><a href="mujer">  <FONT SIZE=5> Mujer</font></a></li> 
                    <li><a href="hombre"> <FONT SIZE=5> Hombre</font></a></li> 
                    <li><a href="ninio"> <FONT SIZE=5> Niño</font></a></li>                      
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    <li><a href="tienda"><span class="glyphicon glyphicon-log-out" aria-hidden="true"></span> Log out</a></li>
                 </ul>
            </div>
        </div>
    </nav>

    

    <div class="container">
    <div class="row">
        <div class="col-md-6">
            <div class="wrap">
                 <img src="img/main.png">  
            </div>
        </div>
    </div>
</div>

    

    @yield('content')
    

    <!-- JavaScripts -->
    
    <script language="javascript" type="text/javascript" src="jquery/jquery-1.12.1.js"></script>
    
        
    
</body>
</html>

<style type="text/css">
.wrap
{
    width: 100%;
    height: 100%;
    min-height: 100%;
    position: absolute;
    
    top: 50%;
    margin-top: 100px;
    left: 0;
    z-index: 99;
}

</style>