<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title></title>

    <!-- Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!--<link href='https://fonts.googleapis.com/css?family=Signika' rel='stylesheet' type='text/css'> -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Tangerine">
    <style>
      body {
        font-family: 'Tangerine', serif;
        font-size: 48px;
      }
    </style>

    <!-- Styles -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>


</head>
<body id="app-layout">

    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <!-- Branding Image -->
                <img alt="Brand" src="img/logo.jpg" height="60" width="75">
                
            </div>

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                <li><p class="navbar-text"> </p></li>
                    <li><a href="mujer"> <FONT SIZE=5> Mujer</font></a></li>  
                    <li><a href="hombre"> <FONT SIZE=5> Hombre</font></a></li> 
                    <li><a href="ninio"> <FONT SIZE=5> Niño</font></a></li>                     
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    <li><a href="tienda"><span class="glyphicon glyphicon-log-out" aria-hidden="true"></span> Log out</a></li>
                 </ul>
            </div>
        </div>
    </nav>

    <div style="margin-left:300px; margin-right:300px; margin-bottom:200px;">
        <form class="compra" method="post" action="https://www.paypal.com/cgi-bin/webscr" name="_xclick">
            <input type="hidden" name="cmd" value="_xclick">
            <input type="hidden" name="upload" value="1">
            <input type="hidden" name="business" value="onlybags113@gmail.com">
            <input type="hidden" name="currency_code" value="EUR">
            <p name="nombre_cliente"> {{$cliente -> nombre }}</p>
            <p name="direccion_cliente"> {{$cliente -> direccion }}</p>
            <p name="telefono_cliente"> {{$cliente -> telefono }}</p>
            <p name="email_cliente"> {{$cliente -> email }}</p><p align="right"> TOTAL: {{$carrito -> precio_total}} € </p>
            </br>

            <div class="table-responsive">
                <table class="table table-condensed">
                    <thead>
                        <tr>
                            <th class="col-sm-4" align="center">Foto</th>
                            <th class="col-sm-6" align="center">Nombre</th>
                            <th class="col-sm-4" align="center">Precio</th>
                            <th class="col-sm-2" align="center">Cantidad</th>
                            <th class="col-sm-2" align="center">Subtotal</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($lineas as $linea)
                            <tr>
                                <td> <img src="{{ $linea['img'] }}" height="100" width="100"></td>
                                <td> {{ $linea['nombre'] }} </td>
                                <td align="center">{{ $linea['precio'] }} € </td>
                                <td align="right">{{ $linea['unidades'] }}</td>
                                <td align="right">{{ $linea['subtotal'] }} €</td>    
                            </tr>
                            <input type="hidden" name="item_name_{{ $linea['nombre'] }}" value="{{ $linea['nombre'] }}">
                            <input type="hidden" name="amount_{{ $linea['subtotal'] }}" value="{{ $linea['subtotal'] }}"> 
                        @endforeach
                    </tbody>
                </table>
            </div>
            <input type="image" src="http://www.paypal.com/es_ES/i/btn/x-click-but01.gif" border="0" name="submit" alt="Realice pagos con PayPal: es rápido, gratis y seguro">    
        </form>
    </div>


    @yield('content')
    

    <!-- JavaScripts -->
    
    <script language="javascript" type="text/javascript" src="jquery/jquery-1.12.1.js"></script>
    
        
    
</body>
</html>